/**
 * 
 */

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;

/**
 * @author Edson Cunha <edson.cunha@eldorado.org.br>
 *
 */
public class Exercicio2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Lê um texto do teclado
		imprime("Digite um texto:");
		String texto = lerTexto();
		
		//Quebra o texto em palavras
		String[] palavras = texto.split(" ");
		
		//Conta as ocorrências de cada palavra
		HashMap<String, Integer> dicionario = new HashMap<String, Integer>();
		
		for (int i=0; i<palavras.length; i++) {
			String palavra = palavras[i];
			
			int ocorrencias_da_palavra = dicionario.getOrDefault(palavra.toLowerCase(), 0);
			
			//esta é uma forma resumida de escrever ocorencias_da_palavra = ocorrencias_da_palavra + 1
			ocorrencias_da_palavra++; 
			
			dicionario.put(palavra, ocorrencias_da_palavra);
		}
		
		//Imprime a quantidade de ocorrências de cada palavra
		imprime("Quantidades encontradas por palavra:");
		
		for (String palavra : dicionario.keySet()) {
			int ocorrencias = dicionario.get(palavra);
			imprime(palavra + ": " + ocorrencias);
		}
	}
	
	public static String lerTexto() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = "";
		try {
			s = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static void imprime(String mensagem) {
		System.out.println(mensagem);
	}
}
