/**
 * 
 */
package exercicio1;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Edson Cunha <edson.cunha@eldorado.org>
 * 
 */
public class Exercicio1 {

	public static float TaxaDeCambio;

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// primeira etapa: lê a taxa de câmbio
		ler("Digite a taxa de câmbio:");
	}

	public static float lerFloat() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		float s = 0;
		try {
			String temp = br.readLine();
			s = Float.parseFloat(temp);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static void imprime(String mensagem) {
		System.out.println(mensagem);
	}
	
	public static void imprime(int numero) {
		System.out.println(numero);
	}

}
