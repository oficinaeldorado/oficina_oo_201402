import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Stack;

/**
 * @author Edson Cunha <edson.cunha@eldorado.org.br>
 *
 */
public class Exercicio1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		
		Stack<String> pilha = new Stack<String>();
		
		//O usuário alimenta a pilha até que ele digite uma linha vazia.
		String linha_lida = " ";
		
		do {
			
			imprime("Digite algo para inserir na posição [" + pilha.size() + "] da pilha:");
			linha_lida = lerTexto();
			
			if (linha_lida.length() > 0)
				pilha.push(linha_lida);
			
		} while (linha_lida.length() > 0);
		
		
		int elemento_a_remover = 0;
		
		//Pergunta ao usuário qual elemento ele quer remover.
		do
		{
			imprime("Digite a posição do item que você quer remover da pilha (0 a " + (pilha.size() - 1) + "):");	
			elemento_a_remover = lerInteiro();	
		} while (elemento_a_remover < 0 || elemento_a_remover >= pilha.size());
		
		
		//Remove o elemento solicitado pelo usuário, respeitando a regra de uso de uma pilha.
		Stack<String> pilhaAuxiliar = new Stack<String>();
		
		int quantidade_de_casas = pilha.size() - elemento_a_remover - 1;
		
		//guarda os itens da pilha para chegar até a posição que desejamos.
		for (int i=0; i< quantidade_de_casas; i++) {
			pilhaAuxiliar.push(pilha.pop());
		}
		
		//remove o elemento da pilha...
		String elemento_removido = pilha.pop();
		imprime("Elemento excluído: " + elemento_removido);
		
		//guarda de volta na pilha os itens que guardamos
		while (!pilhaAuxiliar.empty()) {
			pilha.push(pilhaAuxiliar.pop());
		}
		
		//Imprime a pilha sem o item removido.
		//ATENÇÃO! Vamos imprimir os elementos da pilha NA ORDEM QUE ELES FORAM INSERIDOS!
		//Para fazer isso, o macete é simples: vamos passar de uma pilha para outra para imprimir na ordem inversa!
		while (!pilha.empty()) {
			pilhaAuxiliar.push(pilha.pop());
		}
		
		//Agora, vamos ao mesmo tempo esvaziar a pilha e imprimir os elementos.
		imprime("Como a pilha ficou:");
		while (!pilhaAuxiliar.empty()) {
			imprime( pilhaAuxiliar.pop() );
		}
	}
	
	public static String lerTexto() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String s = "";
		try {
			s = br.readLine();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static int lerInteiro() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int s = 0;
		try {
			String temp = br.readLine();
			s = Integer.parseInt(temp);
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s;
	}
	
	public static void imprime(String mensagem) {
		System.out.println(mensagem);
	}

}
