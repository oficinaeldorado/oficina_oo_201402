/**
 * 
 */
package exercicio2;

//Importa os pacotes necessários para a função lerInteiro()
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Edson Cunha <edson.cunha@eldorado.org.br>
 * 
 */
public class Exercicio2 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int nota;
		String conceito;
		
		imprime("Digite uma nota:");

		nota = lerInteiro();

		if (nota >= 90) {
			conceito = "A";
		} else if (nota >= 80 && nota < 90) {
			conceito = "B";
		} else if (nota >= 70 && nota < 80) {
			conceito = "C";
		} else if (nota >= 60 && nota < 70) {
			conceito = "D";
		} else if (nota >= 50 && nota < 60) {
			conceito = "E";
		} else {
			conceito = "F";
		}

		imprime("O conceito é " + conceito);
	}

	public static void imprime(String mensagem) {
		System.out.println(mensagem);
	}

	public static int lerInteiro() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int s = 0;
		try {
			s = Integer.parseInt(br.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s;
	}

}
