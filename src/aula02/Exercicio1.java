/**
 * 
 */
package exercicio1;

/**
 * @author Edson Cunha <edson.cunha@eldorado.org.br>
 *
 */
public class Exercicio1 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		//Declara um número inteiro e o inicializa com o valor 2.
		int numero = 2;
		
		while (numero <= 14) {
			imprime(numero);
			
			//Incrementa o número em duas unidades a cada volta do laço.
			numero = numero + 2;
		}
	}
	
	public static void imprime(int num) {
		System.out.println(num);
	}

}
