/**
 * 
 */
package exercicio3;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

/**
 * @author Edson Cunha <edson.cunha@eldorado.org.br>
 *
 */
public class Exercicio3 {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		int idadeLida = 0;
		int somaDasIdades = 0;
		int quantidadeDePessoas = 0;
		
		float media = 0;
		
		//primeira etapa: coletamos a soma das idades e a quantidade de pessoas.
		do {
			imprime("Digite uma idade:");
			
			idadeLida = lerInteiro();
			
			somaDasIdades = somaDasIdades + idadeLida;
			quantidadeDePessoas = quantidadeDePessoas + 1;
		} while (idadeLida != 0);
		
		//segunda etapa: calcular a média
		media = somaDasIdades / quantidadeDePessoas;
		
		
		//terceira etapa: diz se a turma é infantil, adolescente, adulta ou idosa
		if (media < 13) {
			imprime("A turma é infantil");
		}
		else if (media >= 13 && media < 19) {
			imprime("A turma é adolescente");
		}
		else if (media >= 19 && media < 65) {
			imprime("A turma é adulta");
		}
		else {
			imprime("A turma é idosa");
		}
	}
	
	public static void imprime(String mensagem) {
		System.out.println(mensagem);
	}
	
	public static int lerInteiro() {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		int s = 0;
		try {
			s = Integer.parseInt(br.readLine());
		} catch (IOException e) {
			e.printStackTrace();
		}
		return s;
	}

}
