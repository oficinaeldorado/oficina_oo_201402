# README #

Lógica, Java e Orientação a Objetos - 2014/02.

### Para que serve este repositório? ###

* Este repositório contém as respostas dos exercícios dados em sala de aula.
* Para acessá-lo, você precisará instalar um cliente do Git em seu computador.
* Caminho do repositório para cloná-lo com o Git: https://<seu usuário>@bitbucket.org/oficinaeldorado/oficina_oo_201402.git
substitua <seu usuário> por seu login no BitBucket.org
